<?php

/**
 * A mail sending implementation that captures sent messages to a variable.
 *
 * This class is for running tests or for development.
 */
class DevelMailSystem extends DefaultMailSystem implements MailSystemInterface {
  /**
   * Accept an e-mail message and store it in a variable.
   *
   * @param $message
   *   An e-mail message.
   */
  public function mail(array $message) {
    // Display the e-mail.
    if (variable_get('devel_mail_display', 1)) {
      dpm($message, 'mail');
    }

    // Save the e-mail.
    if (variable_get('devel_mail_log', 1)) {
      $record = array(
        'message_id' => $message['id'],
        'message_from' => $message['from'],
        'message_to' => $message['to'],
        'language' => $message['language']->language,
        'subject' => truncate_utf8($message['subject'], 255),
        'body' => $message['body'],
        'headers' => $message['headers'],
        'message_sent' => REQUEST_TIME,
      );
      drupal_write_record('devel_mail', $record);
    }

    return TRUE;
  }
}
